use std::fs::File;
use std::io::BufReader;
use std::io::Write;

use anyhow::Context;
use anyhow::Result;

mod config;
mod download;
mod template;
mod server;

pub use config::Config;
pub use config::CommandConfig;

use config::ServeCommandConfig;

use crate::template::TemplateEngine;

pub fn create_context() -> Result<()> {
	let default_context = include_bytes!("../default_context.json");

	let mut file = File::create("context.json")?;
	file.write_all(default_context)?;

	Ok(())
}

pub async fn run_server(serve_config: &ServeCommandConfig) -> Result<()> {
	// Create a temporary directory to store the repo in
	let temp_dir = tempfile::Builder::new().prefix("config_server_").tempdir()?;

	// Download the repo and extract it to the temp dir
	println!("Downloading repository");
	download::download_repo(serve_config.repo_url.as_str(), temp_dir.path()).await?;

	// Determine the path of the configs
	let config_dir = temp_dir.path().join("devops-master").join("configs");

	// Load the template engine
	let templates = TemplateEngine::new(&config_dir);

	let file = File::open("context.json").context("Failed to open file 'context.json'")?;
	let context_reader = BufReader::new(file);
	let context: serde_json::Value = serde_json::from_reader(context_reader)?;

	server::run(serve_config, templates, context).await;

	Ok(())
}
