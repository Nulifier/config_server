use std::env::Args;
use clap::{App, Arg, SubCommand, crate_description, crate_name, crate_version};

const REPO_URL: &str = "https://gitlab.com/Nulifier/devops/-/archive/master/devops-master.tar.gz";

pub struct ServeCommandConfig {
	pub repo_url: String,
	pub port: u16
}

pub enum CommandConfig {
	None,
	Context,
	Serve(ServeCommandConfig)
}

pub struct Config {
	pub command: CommandConfig
}

impl Config {
	pub fn new(args: Args) -> Config {
		let matches = App::new(crate_name!())
			.version(crate_version!())
			.about(crate_description!())
			.subcommand(SubCommand::with_name("context")
				.help("Creates a default context object"))
			.subcommand(SubCommand::with_name("serve")
				.arg(Arg::with_name("repo")
					.help("Specifies the URL that the repository can be downloaded from")))
				.arg(Arg::with_name("port")
					.help("What port the serve will be listening on"))
			.get_matches_from(args);

		let command: CommandConfig = match matches.subcommand() {
			("context", Some(_config_matches)) => {
				CommandConfig::Context
			}
			("serve", Some(serve_matches)) => {
				CommandConfig::Serve(ServeCommandConfig {
					repo_url: serve_matches.value_of("repo").unwrap_or(REPO_URL).to_string(),
					port: serve_matches.value_of("port").unwrap_or("3030").to_string().parse::<u16>().map_err(|_| "Error parsing port argument").unwrap()
				})
			}
			("", None) => {
				CommandConfig::None
			}
			_ => unreachable!()
		};

		Config {
			command
		}
	}
}
