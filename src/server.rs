use std::sync::Arc;
use warp::Filter;
use warp::Reply;
use warp::reply;
use warp::http::StatusCode;

use crate::config::ServeCommandConfig;
use crate::template::TemplateEngine;

pub async fn run(config: &ServeCommandConfig, _templates: TemplateEngine<'static>, _context: serde_json::Value) {
	let hb = Arc::new(_templates);

	let route = warp::path::peek()
		.map(move |peek: warp::filters::path::Peek| {
			let hb = hb.clone();
			println!("GET: {}", peek.as_str());
			match hb.render(peek.as_str(), &_context) {
				Ok(result) => reply::with_status(result, StatusCode::OK).into_response(),
				Err(_e) => reply::with_status("Not Found", StatusCode::NOT_FOUND).into_response()
			}
		});
	
	println!("Starting server on port {}", config.port);
	warp::serve(route)
		.run(([0, 0, 0, 0], config.port))
		.await;
}
