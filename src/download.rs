use flate2::bufread::GzDecoder;
use std::io::BufReader;
use std::path::Path;
use tar::Archive;
use anyhow::Result;

pub async fn download_repo<P: AsRef<Path>>(url: &str, path: P) -> Result<()> {
	// Download the file
	let resp = reqwest::get(url).await?.error_for_status()?;

	// Convert the response into an AsyncRead
	let body = resp.bytes().await?;

	// Decompress the file
	let tar = GzDecoder::new(BufReader::new(body.as_ref()));

	let mut archive = Archive::new(tar);

	archive.unpack(path)?;

	Ok(())
}
