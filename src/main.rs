use anyhow::{Result, anyhow};
use config_server::{CommandConfig, Config, create_context, run_server};

#[tokio::main]
async fn main() {
	let config = Config::new(std::env::args());

	let res = match config.command {
		CommandConfig::Context => create_context(),
		CommandConfig::Serve(serve_config) => run_server(&serve_config).await,
		CommandConfig::None => Err(anyhow!("No command specified"))
	};

	if let Result::Err(e) = res {
		eprintln!("{}", e);
	}
}
