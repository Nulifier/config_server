use std::path::Path;
use handlebars::{Context, Handlebars, Helper, HelperResult, Output, RenderContext, RenderError};
use pwhash::sha512_crypt;
use serde::Serialize;
use anyhow::Result;

pub struct TemplateEngine<'a> {
	handlebars: Handlebars<'a>
}

impl<'a> TemplateEngine<'a> {
	pub fn new<P>(templates_path: P) -> TemplateEngine<'a> where P: AsRef<Path> {
		let mut handlebars = Handlebars::new();

		handlebars.register_helper("shadow", Box::new(shadow_helper));

		handlebars.register_templates_directory("", templates_path).unwrap();

		TemplateEngine {
			handlebars
		}
	}

	pub fn render<T>(&self, name: &str, context: &T) -> Result<String> where T: Serialize {
		Ok(self.handlebars.render(name, context)?)
	}
}

fn shadow_helper(
	h: &Helper,
	_: &Handlebars,
	_: &Context,
	_: &mut RenderContext,
	out: &mut dyn Output
) -> HelperResult {
	let param = h.param(0).ok_or_else(|| RenderError::new("Missing parameter"))?;

	let hash = sha512_crypt::hash(param.render())
		.map_err(|e| RenderError::new(e.to_string()))?;

	out.write(hash.as_str())?;

	Ok(())
}
